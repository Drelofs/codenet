@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row p-5">
        <div class="col-4">
            <img src="{{ $user->profile->profileImage() }}" class="rounded-circle w-100">
        </div>
        <div class="col-8 pt-5">
            <div class="d-flex justify-content-between align-items-baseline">
                <div class="d-flex align-items-center pb-3">
                    <div class="h4">{{ $user->username }}</div>
                    <follow-button user-id="{{ $user->id }}" follows="{{ $follows }}"></follow-button>
                </div>
                @if ($user->disabled == 0)
                    @can('update', $user->profile)
                        <a href="/p/create">Add new message</a>
                    @endcan
                @endif
            </div>
            @if ($user->disabled == 0)
                @can('update', $user->profile)
                    <a href="/profile/{{ $user->id }}/edit">Edit profile</a>
                @endcan
            @endif
            <div class="d-flex">
                <div class="pr-4"><strong>{{ $postCount }}</strong> messages</div>
                <div class="pr-4"><strong>{{ $followersCount }}</strong> followers</div>
                <div class="pr-4"><strong>{{ $followingCount}}</strong> following</div>
            </div>
            <div class="pt-4 font-weight-bold">{{ $user->profile->title}}</div>
            <div>{{ $user->profile->description}}</div>
            <div><a href="www.google.nl">{{$user->profile->url}}</a></div>
        </div>
    </div>

    <div class="row pt-5 pb-5">
        @if($user->disabled == 1)
            <h2>This account is disabled</h2>
        @else
            @foreach($user->posts as $post)
                <div class="col-12 pb-4">
                    <a class="text-dark" href="/p/{{ $post->id }}">
                        <div class="row">
                            <div class="col-12">{{ $post->caption }}</div>
                            @if ($post->image)
                            <div class="col-6">
                                <img src="/storage/{{ $post->image }}" class="w-100">
                            </div>
                            @endif
                        </div>
                    </a>
                </div>
            @endforeach
        @endif
    </div>
</div>
@endsection
