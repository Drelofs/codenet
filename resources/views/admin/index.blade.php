@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Admin panel</h1>
    <p>Users</p>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
            <td><a target="_blank" href="/profile/{{$user->id}}">{{$user->username}}</a></td>
            <td>{{$user->email}}</td>
            <td><disable-button user-id="{{$user->id }}"></disable-button></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection