@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-12 pb-4">
        <div class="row">
            <div class="d-flex align-items-center">
                <div class="pr-3">
                    <img src="{{ $post->user->profile->profileImage() }}" class="rounded-circle w-100" style="max-width:40px;" alt="">
                </div>
                <div>
                    <div class="font-weight-bold">
                        <a href="/profile/{{$post->user->id}}">
                            <span class="text-dark">{{ $post->user->username }}</span>
                        </a>
                        <a href="#" class="pl-3">Follow</a>
                    </div>    
                </div>
            </div>

            <hr> 
            <div class="col-12">{{ $post->caption }}</div>
            @if ($post->image)
            <div class="col-6">
                <img src="/storage/{{ $post->image }}" class="w-100">
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
