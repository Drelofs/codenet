@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($posts as $post)
    <div class="col-12 p-4 mb-4 border">
        <div class="row">
            <div class="d-flex align-items-center">
                <div class="pr-3">
                    <img src="{{ $post->user->profile->profileImage() }}" class="rounded-circle w-100" style="max-width:40px;" alt="">
                </div>
                <div>
                    <div class="font-weight-bold">
                        <a href="/profile/{{$post->user->id}}">
                            <span class="text-dark">{{ $post->user->username }}</span>
                        </a>
                        <a href="#" class="pl-3">Follow</a>
                    </div>    
                </div>
            </div>

            <hr> 
            <div class="col-12">{{ $post->caption }}</div>
            @if ($post->image)
            <div class="col-6">
                <img src="/storage/{{ $post->image }}" class="w-100">
            </div>
            @endif
            <div class="col-6 ml-4">
                @include('posts.commentsDisplay', ['comments' => $post->comments, 'post_id' => $post->id])
                <hr />
                    <h4>Add comment</h4>
                    <form method="post" action="{{ route('comments.store') }}">
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" name="body"></textarea>
                            <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Add Comment" />
                        </div>
                    </form>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
