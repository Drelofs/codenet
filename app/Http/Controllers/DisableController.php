<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class DisableController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function store(User $user)
    {
        $disable_user = User::find($user);
        
        foreach($disable_user as $_user){
            $_user->disabled = !$user->disabled;
            $_user->save();
        }
  
    }
}
