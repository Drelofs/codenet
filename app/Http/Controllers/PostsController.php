<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $users = auth()->user()->following()->pluck('profiles.user_id');

        $posts = Post::whereIn('user_id', $users)->with('user')->latest()->get();

        // dd($posts);

        return view('posts.index', compact('posts'));
    }

    public function admin(){
        $users = User::all();

        return view('admin.index', compact('users'));
    }

    public function create(){
        return view('posts.create');
    }
    public function store(Request $request){

        $data = request()->validate([
            'caption' => 'required',
            'image' => 'image',
        ]);

        $imagepath = ($request->file('image') == null) ? "" : request('image')->store('uploads', 'public'); 

        auth()->user()->posts()->create([
            'caption' => $data['caption'],
            'image' => $imagepath,
        ]);

        return redirect('/profile/'. auth()->user()->id);
    }
    public function show(\App\Post $post){
        return view('posts.show', compact('post'));
    }
}
